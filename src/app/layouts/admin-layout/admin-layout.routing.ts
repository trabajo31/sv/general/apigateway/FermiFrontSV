import { Routes } from '@angular/router';

import { HomeComponent } from '../../pages/home/home.component';
import { UsersComponent } from '../../pages/users/users.component';
import { ClientsComponent } from '../../pages/clients/clients.component';
import { SvActionsComponent } from '../../pages/virtual-sv/sv-actions.component';

export const AdminLayoutRoutes: Routes = [
    { path: 'home',           component: HomeComponent },
    { path: 'users',          component: UsersComponent },
    { path: 'clients',        component: ClientsComponent },
    { path: 'monitoring',     component: SvActionsComponent }
];
