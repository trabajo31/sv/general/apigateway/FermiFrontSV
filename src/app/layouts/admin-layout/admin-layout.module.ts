import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatButtonModule } from '@angular/material/button';
import { MatInputModule } from '@angular/material/input';
import { MatRippleModule } from '@angular/material/core';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatCardModule } from '@angular/material/card';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatSelectModule } from '@angular/material/select';
import { MatDialogModule } from '@angular/material/dialog';
import { DataTablesModule } from 'angular-datatables';
import { AdminLayoutRoutes } from './admin-layout.routing';
import { UsersComponent } from '../../pages/users/users.component';
import { ClientsComponent } from '../../pages/clients/clients.component';
import { HomeComponent } from '../../pages/home/home.component';
import { SvActionsComponent } from '../../pages/virtual-sv/sv-actions.component';
import { CreateEditUsersComponent } from '../../pages/users/modal/create-edit-users/create-edit-users.component';
import { CreateEditClientsComponent } from '../../pages/clients/modal/create-edit-clients/create-edit-clients.component';
import { ComponentsModule } from '../../components/components.module';


@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatRippleModule,
    MatFormFieldModule,
    MatInputModule,
    MatCardModule,
    MatSelectModule,
    MatDialogModule,
    DataTablesModule,
    MatTooltipModule,
    ComponentsModule
  ],
  declarations: [
    HomeComponent,
    UsersComponent,
    ClientsComponent,
    SvActionsComponent,
    CreateEditUsersComponent,
    CreateEditClientsComponent
  ],
  entryComponents:[
    CreateEditUsersComponent,
    CreateEditClientsComponent
  ]
})

export class AdminLayoutModule {}
