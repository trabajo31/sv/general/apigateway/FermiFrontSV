import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../services/login.service';
import { AlertService } from '../../services/utils/alert.service';

declare const $;
declare interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}
export const ROUTESADMIN: RouteInfo[] = [
    { path: '/home', title: 'Inicio',  icon: 'home', class: '' },
    { path: '/monitoring', title: 'Información SV',  icon:'swap_horiz', class: '' },
    { path: '/clients', title: 'Clientes',  icon:'folder_shared', class: '' },
    { path: '/users', title: 'Usuarios',  icon:'people', class: '' }
];

export const ROUTES: RouteInfo[] = [
    { path: '/home', title: 'Inicio',  icon: 'home', class: '' },
    { path: '/monitoring', title: 'Información SV',  icon:'swap_horiz', class: '' },
    { path: '/aplications', title: 'Aplicaciones',  icon:'desktop_windows', class: '' },
    { path: '/logs-change', title: 'Logs Cambios',  icon:'format_list_bulleted', class: '' }
];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html'
})
export class SidebarComponent implements OnInit {
  menuItems;

  constructor(
    private loginService: LoginService, 
    private alertService: AlertService
    ) {/** */ }

  ngOnInit() {
    this.menuItems = ROUTESADMIN.filter(menuItem => menuItem);
  }

  isMobileMenu() {
      if ($(window).width() > 991) {
          return false;
      }
      return true;
  }

  logOut() {
    this.alertService.confirm(
      "Cerrar Sesion", 
      "¿Esta seguro que desea finalizar sesion?"
    ).then((res) => {
      if (res) { 
        this.loginService.logout().subscribe(function(){
            localStorage.clear();
            location.reload();
          });
      }
    });
  }
}
