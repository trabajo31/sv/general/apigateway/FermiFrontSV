import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { 
  userValidator, nameValidator, validateXSS 
} from '../../../../services/utils/validations/form-validations.directive';
import { UsersService } from '../../../../services/users.service';
import { NotifyService } from '../../../../services/utils/notify.service';
import { UserModel } from '../../../../services/utils/models/UserModel';

@Component({
  selector: 'app-create-edit-users',
  templateUrl: './create-edit-users.component.html'
})

export class CreateEditUsersComponent implements OnInit {
  public clientForm: FormGroup;
  loadingDt: boolean = false;
  formGroup : FormGroup;
  control = new  FormControl();
  name_action = false;
  dataEdit: UserModel;
  constructor(
    private formBuild: FormBuilder,
    private usersService: UsersService,
    private notifyService: NotifyService,
    private _router: Router,
    private dialogRef: MatDialogRef<CreateEditUsersComponent>,
    @Inject(MAT_DIALOG_DATA) private data
  ) { }

  ngOnInit(): void {
    this.name_action = this.data.name_action;
    this.buildForm();
  }

  get name() { return this.clientForm.get('name'); }
  get user_network () { return this.clientForm.get('user_network'); }

  get client() {
    return this.clientForm.get('client').value;
  }

  buildForm() {
    this.clientForm = this.formBuild.group({
      name: new FormControl('', [
        Validators.required, Validators.maxLength(200), nameValidator(), validateXSS()
      ]),
      user_network : new FormControl('', [
        Validators.required, Validators.maxLength(100), userValidator(), validateXSS()
      ])
    });
    if(this.data.edit) {
      this.clientForm.controls['name'].setValue(this.data.user.name);
      this.clientForm.controls['user_network'].setValue(this.data.user.user_network);
    }
}

save(): void {
  if(this.clientForm.valid){
    const data = this.clientForm.value;
    if(this.data.edit) {
      
      this.dataEdit.id = this.data.user.id;
      this.dataEdit.name = this.name.value;
      this.dataEdit.user_network = this.user_network.value;

      this.usersService.updateUser(this.dataEdit).subscribe(resp => {
        this.message(resp);
      });
    } else {
      this.usersService.addUser(data).subscribe(resp => {
        this.message(resp);
      });
    }

  }else{

    if(this.clientForm.value.id_client === 'invalid') {
      this.notifyService.error('Seleccione un cliente valido');

    }
    this.notifyService.warning('Ingrese todos los datos');
  }
}

  message(resp):void {
    if (resp.status === 200) {
      this.notifyService.success(resp.message);
      this.dialogRef.close();
      this._router.navigate(['/users']);
    } else {
      this.notifyService.warning(resp.message);
    }
  }
  cancelar(): void {
    this.dialogRef.close();
  }

}
