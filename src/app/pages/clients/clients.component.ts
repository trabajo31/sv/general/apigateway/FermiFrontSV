import { Component, OnInit, ViewChild  } from '@angular/core';
import { Router } from '@angular/router';
import { DataTableDirective } from 'angular-datatables';
import { MatDialog } from '@angular/material/dialog';
import { CreateEditClientsComponent } from './modal/create-edit-clients/create-edit-clients.component';
import { ClientsService } from '../../services/clients.service';
import { AlertService } from '../../services/utils/alert.service';
import { NotifyService } from '../../services/utils/notify.service';
import { DatatablesService } from '../../services/utils/datatables.service';
import { Subject } from 'rxjs';
import { ClientModel } from '../../services/utils/models/ClientModel';

@Component({
  selector: 'app-clients',
  templateUrl: './clients.component.html'
})
export class ClientsComponent implements OnInit {
  @ViewChild(DataTableDirective, {static: false})
  dtElement: DataTableDirective;
  loadingDt: boolean = false;
  clients = [];
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<ClientModel> = new Subject<ClientModel>();

  columsTable = [
    { data: "id" },
    { data: "name" },
    { render: function () {
        return `<button class="btn btn-primary btn-link btn-just-icon mat-raised-button mat-button-base editTbl" mat-raised-button>
                <span class="mat-button-wrapper"><i class="material-icons">mode_edit</i></span>
              </button>
              <button class="btn btn-danger btn-link btn-just-icon mat-raised-button mat-button-base deleteTbl" mat-raised-button>
                <span class="mat-button-wrapper"><i class="material-icons">delete</i></span>
              </button>`;
      } 
    }
  ];

  constructor(
    private alertService: AlertService,
    private notifyService: NotifyService,
    private clientsService: ClientsService,
    private _router: Router,
    private datatablesService: DatatablesService,
    public dialog: MatDialog) { }

  ngOnInit() {
    this.getClients();
  }

  getClients(){
    this.clientsService.getClientAll().subscribe(resp => {
      this.clients = resp.data;
      this.dtTrigger.next();
     });
  }
  buildDataTeble() {
    this.dtOptions = this.datatablesService.config();
  }

  /**
   * @description Funcion para abrir el modal para crear un cliente
   * @author Luis Hernandez <luis.hernandez.ji@grupokonecta.com>
   * @date 21-12-2021
   * @param 
   * @return
   */
  addClient(){
    const dialogRef = this.dialog.open(CreateEditClientsComponent, {
      data: { title: 'Agregar', users: this.clients, name_action : 'Crear' },
      width: '420px',
      disableClose: true
    });
    this.closeDialog(dialogRef);
  }

  /**
   * @description Funcion para abrir el modal para aditar un cliente
   * @author Luis Hernandez <luis.hernandez.ji@grupokonecta.com>
   * @date 21-12-2021
   * @param dataClient el cliente a editar
   * @return
   */
  editClient(dataClient: ClientModel){
    const dialogRef = this.dialog.open(CreateEditClientsComponent, {
      data: { title: 'Editar', client: dataClient, users: this.clients, edit: true, name_action : 'Editar'},
      width: '420px',
      disableClose: true
    });
    this.closeDialog(dialogRef);
  }

  /**
   * @description Funcion para eliminar un cliente
   * @author Luis Hernandez <luis.hernandez.ji@grupokonecta.com>
   * @date 21-12-2021
   * @param dataClient cliente a eliminar
   * @return
   */
  deleteClient(dataClient: ClientModel){
    this.alertService.confirm(
      "Eliminar cliente", 
      "¿Esta seguro que desea eliminar el cliente "+dataClient.name+" ?"
    ).then((res) => {
      if (res) { 
        this.loadingDt = true;
        this.clientsService.deleteClient(dataClient.id).subscribe((resp) => {
          if (resp.status === 200) {
            this.loadingDt = false;
            this.notifyService.success(resp.message);
          } else if (resp.status === 422) {
              var msgerror = '';
              $.each(resp.message, function (indexInArray, valueOfElement) {
                  msgerror += valueOfElement + '<br/>';
              });
              this.alertService.warning('Validacion', msgerror);
          } else {
              this.notifyService.error(resp.message);
          }
        });
      }
      this._router.navigate(['/clients']);
    });
  }

  closeDialog(dialogRef) {
    dialogRef.afterClosed().subscribe((result) => {
      if (result === 'ok') {
        this.renderTable();
      }
    });
  }

  renderTable(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      dtInstance.ajax.reload();
    });
  }

  /**
   * @date 21-12-2021
   * @author Luis Hernandez <luis.hernandez.ji@grupokonecta.com>
   * @description Funcion para abrir el modal para crear un cliente
   * @param {Number} id del cliente a editar
   * @param {Number} category Contiene el id de la categoria de la solicitud
   * @param {Number} status Contiene el id del estado de la solicitud
   * @param {String} description Contiene la descripcion de la solicitud
   */
  detailClient(id) {
      this.dialog.open(CreateEditClientsComponent, {
        data: {
          id
        },
        disableClose: false,
        width: '800px'
      });
  }

}
