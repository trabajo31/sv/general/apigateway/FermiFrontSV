import { Component, OnInit, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { ClientsService } from '../../../../services/clients.service';
import { NotifyService } from '../../../../services/utils/notify.service';
import { virtualServiceValidator, validateXSS } from '../../../../services/utils/validations/form-validations.directive';
import { AlertService } from '../../../../services/utils/alert.service';
import { ClientModel } from '../../../../services/utils/models/ClientModel';

@Component({
  selector: 'app-create-edit-clients',
  templateUrl: './create-edit-clients.component.html'
})
export class CreateEditClientsComponent implements OnInit {
  clienValidate = 'nombre del cliente';
  public clientForm: FormGroup;
  loadingDt: boolean = false;
  formGroup : FormGroup;
  control = new  FormControl();
  name_action = false;
  dataEdit: ClientModel;

  constructor(
    private formBuild: FormBuilder,
    private clientsService: ClientsService,
    private notifyService: NotifyService,
    private _router: Router,
    private alertService: AlertService,
    private dialogRef: MatDialogRef<CreateEditClientsComponent>,
    @Inject(MAT_DIALOG_DATA) private data
  ) { }

  ngOnInit(): void {
    this.name_action = this.data.name_action;
    this.buildForm();
  }

  get name() { return this.clientForm.get('name'); }

  get client() {
    return this.clientForm.get('client').value;
  }

  buildForm() {
    this.clientForm = this.formBuild.group({
      name: new FormControl('', [
        Validators.required, Validators.maxLength(100), validateXSS(),
        virtualServiceValidator(this.clienValidate)
      ])
    });
    if(this.data.edit) {
      this.clientForm.controls['name'].setValue(this.data.client.name);
    }
}

save(): void {
  if(this.clientForm.valid){
    const data = this.clientForm.value;
    if(this.data.edit) {
      this.dataEdit.id = this.data.client.id;
      this.dataEdit.name = data.name;

      this.clientsService.updateClient(this.dataEdit).subscribe(resp => {
        this.message(resp, this.dataEdit);
      });
    } else {
      this.clientsService.addClient(data).subscribe(resp => {
        const clientReactive = {
          'id' : resp.data,
          'name' : data.name
        }; 
        this.message(resp, clientReactive);
      });
    }

  }else{
    if(this.clientForm.value.id_client === 'invalid') {
      this.notifyService.error('Seleccione un cliente valido');

    }
    this.notifyService.warning('Ingrese todos los datos');
  }
}

  message(resp, client):void {
    if (resp.status === 200) {
      this.notifyService.success(resp.message);
      this.dialogRef.close();
      this._router.navigate(['/clients']);
    } else if(resp.status === 202){
      this.reactiveClient(client);
    } else {
      this.notifyService.warning(resp.message);
    }
  }

  cancelar(): void {
    this.dialogRef.close();
  }

  reactiveClient(dataClient: ClientModel){
    this.alertService.confirm(
      "Reactivar cliente", 
      "Este cliente existe en la base de datos ¿Desea reactivar al cliente "+dataClient.name+"?"
    ).then((res) => {
      if (res) { 
        this.loadingDt = true;
        this.clientsService.reactiveClient(dataClient.id).subscribe((resp) => {
          if (resp.status === 200) {
            this.loadingDt = false;
            this.notifyService.success(resp.message);
          } else if (resp.status === 422) {
              var msgerror = '';
              $.each(resp.message, function (indexInArray, valueOfElement) {
                  msgerror += valueOfElement + '<br/>';
              });
              this.alertService.warning('Validacion', msgerror);
          } else {
              this.notifyService.error(resp.message);
          }
        });
      }
      this.dialogRef.close();
      this._router.navigate(['/clients']);
    });
  }
}
