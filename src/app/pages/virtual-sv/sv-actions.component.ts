import { Component, OnInit, ViewChild } from '@angular/core';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs';
import { MatDialog } from '@angular/material/dialog';
import { EditSvComponent } from './modal/edit-sv/edit-sv.component';
import { DelectSvComponent } from './modal/delect-sv/delect-sv.component';
import { CreateSvComponent } from './modal/create-sv/create-sv.component';
import { VirtualServicesDetailComponent } from './modal/virtual-services-detail/virtual-services-detail.component';
import { DatatablesService } from '../../services/utils/datatables.service';
import { UsersService } from '../../services/users.service';
import { Client } from '../../services/utils/models/Client';
import { UserEdit } from '../../services/utils/models/UserEdit';
import { UserCreate } from '../../services/utils/models/UserCreate';
import { ClientsService } from '../../services/clients.service';
import { VirtualService } from '../../services/virtual-sv.service';
import { UserModel } from '../../services/utils/models/UserModel';

@Component({
  selector: 'app-monitoring',
  templateUrl: './sv-actions.component.html'
})
export class SvActionsComponent implements OnInit {

  @ViewChild(DataTableDirective, {static: false})
  dtElement: DataTableDirective;
  loadingDt: boolean = false;
  ClientService: Client;
  UserCreate: UserCreate = new UserCreate();
  UserEdit: UserEdit = new UserEdit();
  user: UserModel;
  aplications = [];
  usersNameSV: string[] = [];
  aplicationsIni = [];
  AllClients: Client[] = [];
  dtOptions: DataTables.Settings = {};
  /** 
   * INFORMACION AL EQUIPO DE SEGUIRAD: 
   * Asi es el llamado paquete, no existe otro metodo para este paquete
   */
  dtTrigger: Subject<any> = new Subject<any>();

  constructor(
    private clientsService: ClientsService,
    private vitrualService: VirtualService,
    private datatablesService: DatatablesService,
    private usersService: UsersService,
    public dialog: MatDialog) { }

  ngOnInit(): void {
    this.user = this.usersService.getUser();

    this.getAplications();
    this.buildDataTeble();
    this.getAllClient();
    this.getUsersSV();
  }

  buildDataTeble() {
    this.dtOptions = this.datatablesService.config();
  }

  addApp(clients){
    const usernamesService = this.usersNameSV;
    this.dialog.open(CreateSvComponent, {
      disableClose: false,
      data: { 
        clients,
        usernamesService
      },
      width: '800px'
    });
  
  }

  getUsersSV(){
    this.vitrualService.getAllUserSV().subscribe(response => {
      if(response.status === 200) {
        this.usersNameSV = response.data;
      }
    });
  }

  getAplications(){
    this.loadingDt = true;
    this.vitrualService.getAplicationsAll().subscribe((resp) => {
      this.loadingDt = false;
      this.aplications = resp.data;
      this.aplicationsIni = resp.data;
      this.ClientService = resp['client'];
      this.UserCreate = resp['user_create'];
      this.UserEdit = resp['user_edit'];
      this.dtTrigger.next();
    });
  }

  getAllClient() {
    this.loadingDt = true;
    let clien = null;
    this.clientsService.getClientAll().subscribe((resp) => {
      this.loadingDt = false;
      this.AllClients = resp.data;
      clien = resp.data;
    });
    return clien;
  }

  renderTable(): void {
    setTimeout(() => {
      this.aplications = this.aplicationsIni;
      this.dtTrigger.next();
    },10);
  }

  /**
   * @date 21-12-2021
   * @author Luis Hernandez <luis.hernandez.ji@grupokonecta.com>
   * @description Funcion para abrir el modal de crear gestión.
   * @param {Number} id Contiene el id de la solicitud a editar
   * @param {Number} category Contiene el id de la categoria de la solicitud
   * @param {Number} status Contiene el id del estado de la solicitud
   * @param {String} description Contiene la descripcion de la solicitud
   */
    detailVirtualService(id) {
    this.dialog.open(VirtualServicesDetailComponent, {
        data: {
            id
            // usernamesService
        },
        disableClose: false
    });

  }

  /**
   * @date 21-12-2021
   * @author Luis Hernandez <luis.hernandez.ji@grupokonecta.com>
   * @description Funcion para abrir el modal de crear gestión.
   * @param {Number} id Contiene el id de la solicitud a editar
   * @param {Number} category Contiene el id de la categoria de la solicitud
   * @param {Number} status Contiene el id del estado de la solicitud
   * @param {String} description Contiene la descripcion de la solicitud
   */
  editVirtualService(id, AllClients) {
    const usernamesService = this.usersNameSV;
    this.dialog.open(EditSvComponent, {
    data: {
      id,
      AllClients,
      usernamesService
    },
    disableClose: false
  });

  }

  /**
   * @date 21-12-2021
   * @author Luis Hernandez <luis.hernandez.ji@grupokonecta.com>
   * @description Funcion para abrir el modal de crear gestión.
   * @param {Number} id Contiente el id del servicio a eliminar
   * @param {Number} category Contiene el id de la categoria de la solicitud
   * @param {Number} status Contiene el id del estado de la solicitud
   * @param {String} description Contiene la descripcion de la solicitud
   */
  delect(id) {
    this.dialog.open(DelectSvComponent, {
      data: {
        id
      },
      disableClose: false
    });
  }
}
