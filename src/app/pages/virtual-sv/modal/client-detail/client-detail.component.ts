import { Component, OnInit , Inject} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AplicationsService } from '../../../../services/aplications.service';
import { DataVirtualService } from '../../../../services/utils/models/DataVirtualService';
import { Client } from '../../../../services/utils/models/Client';
import { UserEdit } from '../../../../services/utils/models/UserEdit';
import { UserCreate } from '../../../../services/utils/models/UserCreate';

@Component({
  selector: 'app-client-detail',
  templateUrl: './client-detail.component.html',
})
export class ClientDetailComponent implements OnInit {
  loadingDt: boolean = false;
  virtualService: DataVirtualService;
  ClientService: Client;
  UserCreate: UserCreate = new UserCreate();
  UserEdit: UserEdit = new UserEdit();
  panelOpenState = false;

  constructor(
    private aplicationsService: AplicationsService,
    private dialogRef: MatDialogRef<ClientDetailComponent>,
    @Inject(MAT_DIALOG_DATA) private data
  ) { }

  ngOnInit(): void {
    //
  }

  save(): void {      
    this.dialogRef.close();
  }

  cancelar(): void {
    this.dialogRef.close();
  }
}
