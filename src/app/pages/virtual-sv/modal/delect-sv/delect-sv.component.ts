import { Component, OnInit , Inject} from '@angular/core';
import { Router } from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AplicationsService } from '../../../../services/aplications.service';
import { DataVirtualService } from '../../../../services/utils/models/DataVirtualService';
import { Client } from '../../../../services/utils/models/Client';
import { UserEdit } from '../../../../services/utils/models/UserEdit';
import { UserCreate } from '../../../../services/utils/models/UserCreate';
import { VirtualService } from '../../../../services/virtual-sv.service';
import { NotifyService } from '../../../../services/utils/notify.service';

@Component({
  selector: 'app-delect-sv',
  templateUrl: './delect-sv.component.html',
})
export class DelectSvComponent implements OnInit {
  loadingDt: boolean = false;
  virtualService: DataVirtualService;
  ClientService: Client;
  user_create: UserCreate = new UserCreate();
  UserEdit: UserEdit = new UserEdit();
  panelOpenState = false;
  Virtual = [];
  showPassword = false;

  constructor(
    private aplicationsService: AplicationsService,
    private serviceSV: VirtualService,
    private notifyService: NotifyService,
    private _router: Router,
    private dialogRef: MatDialogRef<DelectSvComponent>,
    @Inject(MAT_DIALOG_DATA) private data
  ) { }

  ngOnInit(): void {
    this.virtualService = this.data.id;
    this.ClientService = this.data.id.client;
    this.user_create = this.data.id.user_create;
    this.UserEdit = this.data.id.user_edit;
  }

  delect(): void {    
    const idService = {'id': this.data.id.id };
    this.serviceSV.delecteSV(idService).subscribe(res => {
      const status = res.status;
      if (status === 200) {
        this.notifyService.success(res['mensaje']);
        this.dialogRef.close();
        this._router.navigate(['/monitoring']);
      } else {
        this.notifyService.warning(res['mensaje']);
      }
    });
  }

  cancelar(): void {
    this.dialogRef.close();
  }
}
