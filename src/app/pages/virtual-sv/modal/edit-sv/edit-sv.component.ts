import { Component, OnInit , Inject } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { AplicationsService } from '../../../../services/aplications.service';
import { DataVirtualService } from '../../../../services/utils/models/DataVirtualService';
import { VirtualService } from '../../../../services/virtual-sv.service';
import { Observable } from 'rxjs';
import { NotifyService } from '../../../../services/utils/notify.service';
import { validateXSS, nameValidator , validatePassword,
  virtualServiceValidator, clientValidValidator, urlStratrgy, userSvValidator
} from '../../../../services/utils/validations/form-validations.directive';

interface Food {
  value: string;
  viewValue: string;
}

@Component({
  selector: 'app-edit-sv',
  templateUrl: './edit-sv.component.html',
  styleUrls: ['./edit-sv.component.scss']
})
export class EditSvComponent implements OnInit {
  messageUserSV = 'usuario de autenticación'; 
  messageSubject = 'subject'; 
  public virtualServiceForm: FormGroup;
  loadingDt: boolean = false;
  showPassword = false;
  options = ["Sam", "Varun", "Jasmine"];
  formGroup : FormGroup;
  countries: string[] = ['Urugua', 'Polombia', 'Agrandados'];
  clientsName: string[] = [];
  usersNameSV: string[] = [];
  control = new  FormControl();
  filCountries: Observable<string[]>;
  filteredOptions;
  virtualService: DataVirtualService;

  constructor(
    private formBuild: FormBuilder,
    private aplicationsService: AplicationsService,
    private vitrualService: VirtualService,
    private notifyService: NotifyService,
    private _router: Router,
    private dialogRef: MatDialogRef<EditSvComponent>,
    @Inject(MAT_DIALOG_DATA) private data
  ) { }

  ngOnInit(): void {
    this.virtualService = this.data.id;
    this.data.AllClients.forEach(e => {
      this.clientsName.push(e.name);
    });
    this.buildForm();
    this.options = this.clientsName;
    this.filteredOptions = this.clientsName;
    this.getUsersSV();
  }

  get service_user() { return this.virtualServiceForm.get('service_user'); }
  get password() { return this.virtualServiceForm.get('password'); }
  get subject() { return this.virtualServiceForm.get('subject'); }
  get name_servicio() { return this.virtualServiceForm.get('name_servicio'); }
  get service_strategy() { return this.virtualServiceForm.get('service_strategy'); }
  get client() { return this.virtualServiceForm.get('client'); }

  filterData(enteredData){
    this.filteredOptions = this.options.filter(item => {
      return item.toLowerCase().indexOf(enteredData.toLowerCase()) > -1;
    });
  }

  getNames(){
    this.aplicationsService.getData().subscribe(response => {
      this.options = response;
      this.filteredOptions = response;
    });
  }
  private _filter(val: string): string[] {
      const formatVal = val.toLocaleLowerCase();

      return this.countries.filter(country => country.toLocaleLowerCase().indexOf(formatVal) === 0);
  }

  getUsersSV(){
    this.vitrualService.getAllUserSV().subscribe(response => {
      if(response.status === 200) {
        this.usersNameSV = response.data;
      }
    });
  }

  buildForm() {
    this.virtualServiceForm = this.formBuild.group({
      id: new FormControl(this.data.id.id),
      id_client: new FormControl('', [Validators.required]),
      client: new FormControl('', [
        Validators.required, Validators.maxLength(100), clientValidValidator(this.clientsName), validateXSS()
      ]),
      service_user: new FormControl('', [
        Validators.required, Validators.maxLength(100), virtualServiceValidator(this.messageUserSV), 
        validateXSS(), userSvValidator(this.usersNameSV)
      ]),
      password: new FormControl('', [
        Validators.required, validateXSS(), validatePassword(), validateXSS()
      ]),
      subject: new FormControl('', [
        Validators.required, Validators.maxLength(100), 
        virtualServiceValidator(this.messageSubject), validateXSS()
      ]),
      name_servicio: new FormControl('', [
        Validators.required, Validators.maxLength(100), nameValidator(), validateXSS()
      ]),
      service_strategy: new FormControl('', [
        Validators.required, Validators.maxLength(100), urlStratrgy()]),
      delected: new FormControl(0)
    });
    this.virtualServiceForm.get('client').valueChanges.subscribe(response => {
      this.filterData(response);
      const id_client = this.getIdClient(response);
      if(id_client !== null) {
        this.virtualServiceForm.controls['id_client'].setValue(id_client);
      }
    });
    this.setDataService();
}

setDataService() {
  if (this.virtualService !== undefined) {
      this.virtualServiceForm.controls['id_client'].setValue(this.data.id.id_client);
      this.virtualServiceForm.controls['client'].setValue(this.data.id.client.name);
      this.virtualServiceForm.controls['service_user'].setValue(this.data.id.service_user);
      this.virtualServiceForm.controls['password'].setValue(this.data.id.password);
      this.virtualServiceForm.controls['subject'].setValue(this.data.id.subject);
      this.virtualServiceForm.controls['name_servicio'].setValue(this.data.id.name_servicio);
      this.virtualServiceForm.controls['service_strategy'].setValue(this.data.id.service_strategy);
  }
}

getIdClient(nameClient: string) {
  const datas = this.data.AllClients;
  let id = 'invalid';
  datas.forEach(e => {
    if(e.name === nameClient){
      id = e.id;
    }
  });
  return id;
}


save(): void {

  const id_client = this.getIdClient(this.virtualServiceForm.value.client);

  this.virtualServiceForm.controls['id_client'].setValue(id_client);

  this.loadingDt = true;
  if(this.virtualServiceForm.valid && this.virtualServiceForm.value.id_client !== 'invalid'){
    this.loadingDt = false;
    const data = this.virtualServiceForm.value;

    this.vitrualService.updateUpdateSV(data).subscribe(resp => {

      const status = resp.status;
      this.loadingDt = false;

      if (status === 200) {
        this.notifyService.success(resp['mensaje']);
        this.dialogRef.close();
        this._router.navigate(['/monitoring']);
      } else {
        this.notifyService.warning(resp['mensaje']);
      }
    });
  }else{
    if(this.virtualServiceForm.value.id_client === 'invalid') {
      this.notifyService.error('Seleccione un cliente valido');
    }
    this.notifyService.warning('Ingrese todos los datos');
  }
  this.loadingDt = false;
}

  cancelar(): void {
    this.dialogRef.close();
  }

}