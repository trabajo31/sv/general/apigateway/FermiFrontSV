import { Component, OnInit , Inject } from '@angular/core';
import { Router } from '@angular/router';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { VirtualService } from '../../../../services/virtual-sv.service';
import { validateXSS, nameValidator , validatePassword,
  virtualServiceValidator, clientValidValidator, urlStratrgy, userSvValidator
} from '../../../../services/utils/validations/form-validations.directive';
import { Observable } from 'rxjs';
import { NotifyService } from '../../../../services/utils/notify.service';
import { AlertService } from '../../../../services/utils/alert.service';


@Component({
  selector: 'app-create-sv',
  templateUrl: './create-sv.component.html',
})
export class CreateSvComponent implements OnInit {
  messageUserSV = 'usuario de autenticación'; 
  messageSubject = 'subject'; 
  public virtualServiceForm: FormGroup;
  
  loadingDt: boolean = false;
  showPassword = false;
  options = ["Sam", "Varun", "Jasmine"];
  formGroup : FormGroup;
  countries: string[] = ['Urugua', 'Polombia', 'Agrandados'];
  clientsName: string[] = [];
  usersNameSV: string[] = [];
  control = new  FormControl();
  filCountries: Observable<string[]>;
  filteredOptions;

  constructor(
    private formBuild: FormBuilder,
    // private aplicationsService: AplicationsService,
    private vitrualService: VirtualService,
    private notifyService: NotifyService,
    private _router: Router,
    private alertService: AlertService,
    private dialogRef: MatDialogRef<CreateSvComponent>,
    @Inject(MAT_DIALOG_DATA) private data
  ) { }

  ngOnInit(): void {
    this.usersNameSV = this.data.usernamesService;
    this.data.clients.forEach(e => {
      this.clientsName.push(e.name);
    });
    this.buildForm();
    this.options = this.clientsName;
    this.filteredOptions = this.clientsName;
  }

  get service_user() { return this.virtualServiceForm.get('service_user'); }
  get password() { return this.virtualServiceForm.get('password'); }
  get subject() { return this.virtualServiceForm.get('subject'); }
  get name_servicio() { return this.virtualServiceForm.get('name_servicio'); }
  get service_strategy() { return this.virtualServiceForm.get('service_strategy'); }
  get client() { return this.virtualServiceForm.get('client'); }

  filterData(enteredData){
    this.filteredOptions = this.options.filter(item => {
      return item.toLowerCase().indexOf(enteredData.toLowerCase()) > -1;
    });
  }

  getUsersSV(){
    this.vitrualService.getAllUserSV().subscribe(response => {
      if(response.status === 200) {
        this.usersNameSV = response.data;
      }
    });
  }

  private _filter(val: string): string[] {
      const formatVal = val.toLocaleLowerCase();

      return this.countries.filter(country => country.toLocaleLowerCase().indexOf(formatVal) === 0);
  }

  buildForm() {
    this.virtualServiceForm = this.formBuild.group({
      id_client: new FormControl('', [Validators.required]),
      client: new FormControl('', [
        Validators.required, Validators.maxLength(100), clientValidValidator(this.clientsName), validateXSS()
      ]),
      service_user: new FormControl('', [
        Validators.required, Validators.maxLength(100), virtualServiceValidator(this.messageUserSV), 
        validateXSS(), userSvValidator(this.usersNameSV)
      ]),
      password: new FormControl('', [
        Validators.required, validateXSS(), validatePassword(), validateXSS()
      ]),
      subject: new FormControl('', [
        Validators.required, Validators.maxLength(100), 
        virtualServiceValidator(this.messageSubject), validateXSS()
      ]),
      name_servicio: new FormControl('', [
        Validators.required, Validators.maxLength(100), nameValidator(), validateXSS()
      ]),
      service_strategy: new FormControl('', [
        Validators.required, Validators.maxLength(100), urlStratrgy()]),
      delected: new FormControl(0)
    });
    this.virtualServiceForm.get('client').valueChanges.subscribe(response => {
      this.filterData(response);
      const id_client = this.getIdClient(response);
      if(id_client !== null) {
        this.virtualServiceForm.controls['id_client'].setValue(id_client);
      }   
    });
}

getIdClient(nameClient: string) {
  const datas = this.data.clients;
  let id = 'invalid';
  datas.forEach(e => {
    if(e.name === nameClient){
      id = e.id;
    }
  });
  return id;
}


save(): void {    

  const id_client = this.getIdClient(this.virtualServiceForm.value.client);

  this.virtualServiceForm.controls['id_client'].setValue(id_client);

  if(this.virtualServiceForm.valid && this.virtualServiceForm.value.id_client !== 'invalid'){
    const data = this.virtualServiceForm.value;

    this.vitrualService.addVirtualService(data).subscribe(resp => {
      const clientReactive = {
        'id' : resp.data,
        'name' : data.name_servicio
      }; 
      this.message(resp, clientReactive);
    });
  }else{

    if(this.virtualServiceForm.value.id_client === 'invalid') {
      this.notifyService.error('Seleccione un cliente valido');

    }
    this.notifyService.warning('Ingrese todos los datos');
  }
}

message(resp, client):void {
  if (resp.status === 200) {
    this.notifyService.success(resp.message);
        this.dialogRef.close();
        this._router.navigate(['/monitoring']);
  } else if(resp.status === 202){
    this.reactiveSV(client);
  } else {
    this.notifyService.warning(resp.message);
  }
}

reactiveSV(dataClient){
  this.alertService.confirm(
    "Reactivar servicio", 
    "Este servicio virtual ya existe en la base de datos ¿Desea reactivar el servicio "+dataClient.name+"?"
  ).then((res) => {
    if (res) { 
      this.loadingDt = true;
      this.vitrualService.reactivateSV({'id' : dataClient.id}).subscribe((resp) => {
        if (resp.status === 200) {
          this.loadingDt = false;
          this.notifyService.success(resp.message);
        } else if (resp.status === 422) {
            var msgerror = '';
            $.each(resp.message, function (indexInArray, valueOfElement) {
                msgerror += valueOfElement + '<br/>';
            });
            this.alertService.warning('Validacion', msgerror);
        } else {
            this.notifyService.error(resp.message);
        }
      });
    }
    this.dialogRef.close();
    this._router.navigate(['/monitoring']);
  });
}
  cancelar(): void {
    this.dialogRef.close();
  }

}
