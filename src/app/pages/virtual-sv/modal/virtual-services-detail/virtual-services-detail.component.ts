import { Component, OnInit , Inject} from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { AplicationsService } from '../../../../services/aplications.service';
import { DataVirtualService } from '../../../../services/utils/models/DataVirtualService';
import { Client } from '../../../../services/utils/models/Client';
import { UserEdit } from '../../../../services/utils/models/UserEdit';
import { UserCreate } from '../../../../services/utils/models/UserCreate';

@Component({
  selector: 'app-virtual-services-detail',
  templateUrl: './virtual-services-detail.component.html'
})
export class VirtualServicesDetailComponent implements OnInit {
  loadingDt: boolean = false;
  virtualService: DataVirtualService;
  ClientService: Client;
  user_create: UserCreate = new UserCreate();
  UserEdit: UserEdit = new UserEdit();
  panelOpenState = false;
  Virtual = [];
  showPassword = false;

  constructor(
    private aplicationsService: AplicationsService,
    private dialogRef: MatDialogRef<VirtualServicesDetailComponent>,
    @Inject(MAT_DIALOG_DATA) private data
  ) { }

  ngOnInit(): void {
    this.virtualService = this.data.id;
    this.ClientService = this.data.id.client;
    this.user_create = this.data.id.user_create;
    this.UserEdit = this.data.id.user_edit;
  }

  save(): void {      
    this.dialogRef.close();
  }

  cancelar(): void {
    this.dialogRef.close();
  }
}
