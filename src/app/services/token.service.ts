import { Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { UserModel } from './utils/models/UserModel';
import { environment } from '../../environments/environment';

export interface AuthMe {
  status: number;
  data : UserModel;
}

@Injectable({
  providedIn: 'root'
})
export class TokenService {

  constructor(private _router: Router,private http: HttpClient) { }

  userLoggin() {
    const token = localStorage.getItem('AC_TOKEN');
    if (token === undefined || token === null) {
      this._router.navigate(['/login']);
      return false;
    } else {
      this._router.navigate(['/home']);
    }
    return true;
  }

  setToken(token) {
    localStorage.setItem('AC_TOKEN', token);

    if (localStorage.getItem('AC_TOKEN') !== null) {
      return true;
    } else {
      return false;
    }
  }

  getDataUserFromToken(param): Observable<AuthMe> {
    return this.http.post<AuthMe>( environment.urlApi + '/front/auth/me', param).pipe();
  }

  getToken(){
      return localStorage.getItem('AC_TOKEN');
  }
  
  clearToken(){
    localStorage.clear();
  }
}
