
export interface DataVirtualService {   
    created_at: string;
    delected: number;
    id: number;
    id_client: number;
    id_user_create: number;
    id_user_edit: number;
    name_servicio: string;
    password: string;
    service_strategy: string;
    service_user: string;
    subject: string;
    updated_at: string;
    user_create: [];
    user_edit: [];
    client: [];
}