import { UserModel } from "./UserModel";

export class ClientModel {
    public id: number;
    public name: string;
    public id_user_create: number;
    public id_user_edit: number;
    public delected: number;
    public user_create: UserModel;
    public user_edit: UserModel;
}