import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { DataUser } from '../commons/dataUser'; 
import { UserModel } from './utils/models/UserModel'; 
import { ResponseModel } from './utils/models/ResponseModel'; 

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  constructor( private http: HttpClient ) { }

  getUserAll(){
    return this.http.post<ResponseModel>(environment.urlApi +'/front/users/list', {});
  }

  getUserAllList(){
    return this.http.post<ResponseModel>(environment.urlApi +'/users/list', {});
  }

  getUser(){
    return DataUser.user;
  }

  setUser(user: UserModel){
    DataUser.user = user;
  }

  addUser(data:UserModel) {
    return this.http.post<ResponseModel>(environment.urlApi + '/front/users/register', data);
  }

  updateUser(data:UserModel) {
    return this.http.post<ResponseModel>(environment.urlApi + '/front/users/update', data);
  }

  deleteUser(data:UserModel) {
    return this.http.post<ResponseModel>(environment.urlApi + '/users/delete', data);
  }
}
