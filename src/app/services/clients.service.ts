import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { ResponseModel } from './utils/models/ResponseModel';
import { ClientModel } from './utils/models/ClientModel';

@Injectable({
  providedIn: 'root'
})
export class ClientsService {

  constructor( private http: HttpClient ) { }
  
  getClientAll(){
    return this.http.post<ResponseModel>(environment.urlApi + '/front/clients/list', {});
  }

  getClientList(){
    return this.http.post<ResponseModel>(environment.urlApi + '/front/clients/list', {});
  }

  addClient(data: ClientModel) {
    return this.http.post<ResponseModel>(environment.urlApi + '/front/clients/add', data);
  }

  updateClient(data: ClientModel) {
    return this.http.post<ResponseModel>(environment.urlApi + '/front/clients/update', data);
  }

  deleteClient(id) {
    return this.http.post<ResponseModel>(
      environment.urlApi + '/front/clients/delete', 
      { 'id' : id }
    );
  }
  
  reactiveClient(id) {
    return this.http.post<ResponseModel>(
      environment.urlApi + '/front/clients/reactive', 
      { 'id' : id }
    );
  }

}
