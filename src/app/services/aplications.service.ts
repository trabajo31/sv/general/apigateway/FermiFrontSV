import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';
import { ResponseModel } from './utils/models/ResponseModel';

@Injectable({
  providedIn: 'root'
})
export class AplicationsService {

  constructor( private http: HttpClient ) { }

  getData(){
    return this.http.get('https://jsonplaceholder.typicode.com/users')
      .pipe(
        map((response:[]) => response.map(item => item['name']))
      );
  }
  getAplicationsAll(){
    return this.http.post<ResponseModel>(environment.urlApi + '/front/service/list', {});
  }

  getServiceById(id){
    return this.http.post<ResponseModel>(environment.urlApi + '/front/service/search', {id});
  }

  addAplications(data) {
    return this.http.post<ResponseModel>(environment.urlApi + '/aplications/create', data);
  }

  updateAplications(data) {
    return this.http.post<ResponseModel>(environment.urlApi + '/aplications/update', data);
  }

  changeDbAplications(data) {
    return this.http.post<ResponseModel>(environment.urlApi + '/aplications/changeDb', data);
  }

  updateAplicationsDetails(data) {
    return this.http.post<ResponseModel>(environment.urlApi + '/aplications/update/details', data);
  }

  changeStateAplications(data) {
    return this.http.post<ResponseModel>(environment.urlApi + '/aplications/update/state', data);
  }
}
