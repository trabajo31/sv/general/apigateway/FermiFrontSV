import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { ResponseModel } from './utils/models/ResponseModel'; 

@Injectable({
  providedIn: 'root'
})
export class VirtualService {

  constructor( private http: HttpClient ) { }

  getAplicationsAll(){
    return this.http.post<ResponseModel>(environment.urlApi + '/front/service/list', {});
  }

  getAllUserSV(){
    return this.http.post<ResponseModel>(environment.urlApi + '/front/service/listUserSV', {});
  }

  getServiceById(id){
    return this.http.post<ResponseModel>(environment.urlApi + '/front/service/search', {id});
  }

  addVirtualService(data) {
    return this.http.post<ResponseModel>(environment.urlApi + '/front/service/register', data);
  }

  updateUpdateSV(data) {
    return this.http.post<ResponseModel>(environment.urlApi + '/front/service/update', data);
  }

  reactivateSV(data) {
    return this.http.post<ResponseModel>(environment.urlApi + '/front/service/reactive', data);
  }

  delecteSV(data) {
    return this.http.post<ResponseModel>(environment.urlApi + '/front/service/delete', data);
  }

  
}
